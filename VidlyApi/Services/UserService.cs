﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using VidlyApi.DbContexts;
using VidlyApi.Entities;

namespace VidlyApi.Services
{
    public interface IUserService
    {
        bool IsAnExistingUser(string userName);
        bool IsValidUserCredentials(string userName, string password);
        string GetUserRole(string userName, out User userDetal);
        User GetUserByUsername(string username);
    }

    public class UserService : IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly VidlyDbContext _dbContext;

        // inject your database here for user validation
        public UserService(ILogger<UserService> logger, VidlyDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public bool IsValidUserCredentials(string userName, string password)
        {
            _logger.LogInformation($"Validating user [{userName}]");
            if (string.IsNullOrWhiteSpace(userName))
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                return false;
            }

            var dbUser = _dbContext.User.Where(x => x.Email == userName && x.Password == password);
            if (dbUser != null)
                return true;
            return false;
        }

        public bool IsAnExistingUser(string userName)
        {
            var dbUser= _dbContext.User.Where(x=>x.Username == userName);
            if (dbUser != null)
                return true;
            return false;
        }

        public string GetUserRole(string userName, out User userDetail)
        {
            userDetail = GetUserByUsername(userName);
            if (!IsAnExistingUser(userName))
            {
                return string.Empty;
            }

            if (userDetail.IsAdmin)
            {
                return UserRoles.Admin;
            }

            return UserRoles.BasicUser;
        }

        public User GetUserByUsername(string username)
        {
            return _dbContext.User.Where(x => x.Email == username).FirstOrDefault();
        }
    }

    public static class UserRoles
    {
        public const string Admin = nameof(Admin);
        public const string BasicUser = nameof(BasicUser);
    }
}
