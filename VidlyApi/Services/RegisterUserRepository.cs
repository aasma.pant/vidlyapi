﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VidlyApi.DbContexts;
using VidlyApi.Entities;

namespace VidlyApi.Services
{
    public class RegisterUserRepository :IRegisterUserRepository
    {
        private readonly VidlyDbContext _dbContext;
        public RegisterUserRepository(VidlyDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public bool RegisterUser(User user)
        {            
            bool userExists = CheckIfUserExists(user);
            if (userExists)
                return false;
            user.Id = Guid.NewGuid();
            user.IsAdmin = false;
            _dbContext.User.Add(user);
            return Save();
        }

        public bool Save()
        {
            return (_dbContext.SaveChanges() >= 0);
        }

        public bool CheckIfUserExists(User user)
        {
            var dbUser =_dbContext.User.Where(x=>x.Email == user.Email).FirstOrDefault();
            if (dbUser == null)
                return false;
            return true;
        }
    }
}
