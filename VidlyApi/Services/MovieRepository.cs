﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VidlyApi.DbContexts;
using VidlyApi.Entities;
using Microsoft.EntityFrameworkCore;
using VidlyApi.ResourceParameters;

namespace VidlyApi.Services
{
    public class MovieRepository : IMovieRepository
    {
        private readonly VidlyDbContext _dbContext;

        public MovieRepository(VidlyDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }
        public void AddMovie(Movie movie)
        {
            if (movie == null)
                throw new ArgumentNullException(nameof(movie));

            movie.Id = Guid.NewGuid();
            movie.LikeFlag = false;

            _dbContext.Movie.Add(movie);
            Save();
        }

        public bool DeleteMovie(Guid movieId)
        {
            var movie = _dbContext.Movie.Find(movieId);
            _dbContext.Movie.Remove(movie);
            return Save();
        }

        public GetAllMovies GetMovie(Guid Id)
        {
            return _dbContext.GetAllMovies.FromSqlRaw<GetAllMovies>("GetAllMovies {0}",Id).ToList().FirstOrDefault();
        }

        public IEnumerable<GetAllMovies> GetMovies(MovieResourceParameter resourceParameter)
        {
            //string searchQuery = resourceParameter.SearchQuery.ToLowerInvariant().Trim();
            var movieList = _dbContext.GetAllMovies.FromSqlRaw<GetAllMovies>("GetAllMovies").ToList();
            //movieList = movieList.Where(x => x.Title.ToLowerInvariant().Contains(searchQuery)).ToList();
            return movieList;
        }

        public bool Save()
        {
            return (_dbContext.SaveChanges() >= 0);
        }

        public bool UpdateMovie(Movie movie)
        {
            Movie dbMovie = _dbContext.Movie.Find(movie.Id);
            dbMovie.Title = movie.Title;
            dbMovie.PublishDate = movie.PublishDate;
            dbMovie.NumberInStock = movie.NumberInStock;
            dbMovie.LikeFlag = movie.LikeFlag;
            dbMovie.GenreId = movie.GenreId;
            _dbContext.Movie.Update(dbMovie);
            return Save();
        }

        public bool LikeUnlikeMovie(Guid movieId)
        {
            Movie dbMovie = _dbContext.Movie.Find(movieId);
            if (dbMovie.LikeFlag)
                dbMovie.LikeFlag = false;
            else
                dbMovie.LikeFlag = true;
            _dbContext.Movie.Update(dbMovie);
            return Save();
        }
    }
}
