﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VidlyApi.DbContexts;
using VidlyApi.Entities;

namespace VidlyApi.Services
{
    public class GenreRepository : IGenreRepository, IDisposable
    {
        private readonly VidlyDbContext _dbContext;
        public GenreRepository(VidlyDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose resources when needed
            }
        }

        public Genre GetGenre(Guid genreId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Genre> GetGenres()
        {
            return _dbContext.Genre.ToList();
        }
    }
}
