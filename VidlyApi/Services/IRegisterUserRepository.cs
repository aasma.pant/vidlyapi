﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VidlyApi.Entities;

namespace VidlyApi.Services
{
    public interface IRegisterUserRepository
    {
        bool RegisterUser(User user);
        bool CheckIfUserExists(User user);
        bool Save();
    }
}
