﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VidlyApi.Entities;
using VidlyApi.ResourceParameters;

namespace VidlyApi.Services
{
    public interface IMovieRepository
    {
        IEnumerable<GetAllMovies> GetMovies(MovieResourceParameter resourceParameter);

        GetAllMovies GetMovie(Guid Id);

        void AddMovie(Movie movie);

        bool UpdateMovie(Movie movie);

        bool DeleteMovie(Guid movieId);

        bool Save();

        bool LikeUnlikeMovie(Guid movieId);


    }
}
