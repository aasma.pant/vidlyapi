﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VidlyApi.Entities;

namespace VidlyApi.Services
{
    public interface IGenreRepository
    {
        IEnumerable<Genre> GetGenres();

        Genre GetGenre(Guid genreId);


    }
}
