﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using VidlyApi.Entities;
using VidlyApi.ResourceParameters;
using VidlyApi.Services;

namespace VidlyApi.Controllers
{
    [EnableCors("MyOrigins")]
    [Route("api/movie")]
    //[ValidateAntiForgeryToken]
    [ApiController]
    [Authorize]
    public class MovieController : ControllerBase
    {
        private readonly IMovieRepository _movieRepo;

        public MovieController(IMovieRepository movieRepo)
        {
            _movieRepo = movieRepo ?? throw new ArgumentNullException(nameof(movieRepo));
        }

        [HttpGet(Name ="GetAllMovies")]
        public IActionResult GetMoviesCollection([FromQuery]MovieResourceParameter resourceParameter)
        {
            var movieList = _movieRepo.GetMovies(resourceParameter);
            return Ok(movieList);
        }

        [HttpGet("{movieId}",Name ="GetMovie")]
        public IActionResult GetMovie(Guid movieId)
        {
            var movie = _movieRepo.GetMovie(movieId);
            return Ok(movie);
        }

        [HttpPost(Name ="CreateMovie")]
        public ActionResult<Movie> AddMovie(Movie movie)
        {
            if(movie.Id == null || movie.Id == Guid.Empty)
            {
                _movieRepo.AddMovie(movie);
            }
            else
            {
                _movieRepo.UpdateMovie(movie);
            }
            
            return Ok();
            //return CreatedAtRoute("GetMovie", new { movieId = movie.Id }, movie);
        }

        [HttpPut("{movieId}",Name ="LikeUnlikeMovie")]
        public IActionResult LikeUnlikeMovie(Guid movieId)
        {
            _movieRepo.LikeUnlikeMovie(movieId);
            return Ok();
        }

        [HttpDelete("{movieId}" , Name ="DeleteMovie")]
        public IActionResult DeleteMovie(Guid movieId)
        {
            var deleteMovie = _movieRepo.DeleteMovie(movieId);
            return NoContent();
        }

        //[HttpPut("{movieId}", Name ="UpdateMovie")]
        //public ActionResult<GetAllMovies> UpdateMovie(Movie movie)
        //{
        //    _movieRepo.UpdateMovie(movie);
        //    return NoContent();
        //}
    }
}