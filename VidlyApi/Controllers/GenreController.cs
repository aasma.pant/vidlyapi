﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VidlyApi.Services;

namespace VidlyApi.Controllers
{
    [EnableCors("MyOrigins")]
    [Route("api/genre")]
    [ApiController]
    public class GenreController : ControllerBase
    {
        private readonly IGenreRepository _genreRepo;
        public GenreController(IGenreRepository genreRepo)
        {
            _genreRepo = genreRepo ?? throw new ArgumentNullException(nameof(genreRepo));
        }

        [HttpGet]
        public IActionResult GetGenreCollection()
        {
            var genreList = _genreRepo.GetGenres();
            return Ok(genreList);
        }
    }
}