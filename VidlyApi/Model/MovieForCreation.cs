﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VidlyApi.Model
{
    public class MovieForCreation
    {
        public string Title { get; set; }
        public Guid GenreId { get; set; }
        public int NumberInStock { get; set; }
        public double DailyRentalRate { get; set; }
    }
}
