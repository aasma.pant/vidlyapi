﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VidlyApi.Entities
{
    public class GetAllMovies
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int NumberInStock { get; set; }
        public double DailyRentalRate { get; set; }
        public DateTime? PublishDate { get; set; }
        public bool LikeFlag { get; set; }
        public Guid GenreId { get; set; }
    }
}
