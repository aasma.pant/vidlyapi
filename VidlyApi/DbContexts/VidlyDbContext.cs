﻿using System;
using Microsoft.EntityFrameworkCore;
using VidlyApi.Entities;

namespace VidlyApi.DbContexts
{
    public class VidlyDbContext : DbContext
    {
        public VidlyDbContext (DbContextOptions<VidlyDbContext> options): base(options) 
        {}

        public DbSet<Genre> Genre { get; set; }
        public DbSet<Movie> Movie { get; set; }
        public DbSet<GetAllMovies> GetAllMovies { get; set; }
        public DbSet<User> User { get; set; }
    }
}
