﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VidlyApi.Migrations
{
    public partial class GetAllMovies : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string procedure = @" create procedure GetAllMovies (@movieId uniqueidentifier = null)
                                as begin
                                select m.Id,m.Title,m.NumberInStock, m.DailyRentalRate, m.PublishDate,                       m.LikeFlag, g.Name Genre , g.Id GenreId
	                            from Movie m
	                            left join Genre g on g.Id = m.genreId 
                                where @movieId is null or m.Id = @movieId
                                end";

            migrationBuilder.Sql(procedure);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            string procedure = @" Drop procedure GetAllMovies";
            migrationBuilder.Sql(procedure);
        }
    }
}
